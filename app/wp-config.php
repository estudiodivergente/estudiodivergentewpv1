<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'divergenteuser');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'password');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Srkb>*hijdRW wIN|,o^muKwYoum#iGU`O59V,4{9=T*h2kEO/N,cI.<q<Y=y~hQ');
define('SECURE_AUTH_KEY', ':cHH<:C!_44*6A6mY1an@D4:3uI4I{NK<T!cyt}^pRS5Ev>D@ybc2k@ptm{A^!j#');
define('LOGGED_IN_KEY', 'dnt+|bH&U]z/Bkc(o`R8Q`Q/kf^^e=|sWCM%.Y*YVFWF^1tcb;fPhZjHR+0Y{90l');
define('NONCE_KEY', 'b0i8Xrmkaxz6?,S(2y%U?A*ky3s{KxTcO)r}o{rcT7sDl!APa=9Ev5rtx/opesVP');
define('AUTH_SALT', 'gG4KVkbGm)*hYlD(Ll`dpWIYd;XuDfP2R+OhUoLwJ|FW#n5-r?8*.we-Jv[Q2qCG');
define('SECURE_AUTH_SALT', 'b>ifD2{Pb BV<8gpir`NMUI$~Aa{0C&l3,NB$=-vOiK;vtGRyg^1gOe#o.BGw}]_');
define('LOGGED_IN_SALT', '=^PiejpYdwJl/j?78ez8dFoV/_P-q7`Jg`V~pKjsQ{c{s8(X1<>Hd,N5|,JPUr_e');
define('NONCE_SALT', 'cwKk9U:@ o$Hxcjyp4BiII.d5ny47~>}v5iS_.i?7,d!ZM:(hE9P%_6;+u--p_!F');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

