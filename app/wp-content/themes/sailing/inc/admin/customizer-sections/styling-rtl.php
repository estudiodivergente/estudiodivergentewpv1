<?php
$styling->addSubSection( array(
	'name'     => 'RTL Support & Preload',
	'id'       => 'styling_rtl',
	'position' => 14,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'RTL Support',  'sailing' ),
	'id'      => 'rtl_support',
	'type'    => 'checkbox',
	"desc"    => "Enable/Disable",
	'default' => false,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Preload',  'sailing' ),
	'id'      => 'preload',
	'type'    => 'checkbox',
	"desc"    => "Enable/Disable",
    'default' => true,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Import Demo Data',  'sailing' ),
	'id'      => 'enable_import_demo',
	'type'    => 'checkbox',
	"desc"    => "Enable/Disable",
	'default' => true,
) );
