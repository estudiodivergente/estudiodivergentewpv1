<?php
/*
 * Post and Page Display Settings
 */
$display->addSubSection( array(
	'name'     => esc_html__('Page 404', 'sailing'),
	'id'       => 'display_page_404',
	'position' => 6,
	'desc'     => 'it just works with header option: Overlay'
) );

$display->createOption( array(
	'name'        => esc_html__('Top Image', 'sailing'),
	'id'          => '404_top_image',
	'type'        => 'upload',
	'desc'        => 'Enter URL or Upload an top image file for header',
	'livepreview' => ''
) );

$display->createOption( array(
	'name'        => esc_html__('Background Top Heading Color', 'sailing'), 
	'id'          => '404_heading_bg_color',
	'type'        => 'color-opacity',
	'default'     => '#181818',
	'livepreview' => ''
) );
