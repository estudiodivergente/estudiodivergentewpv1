<?php

class Thim_Social_Widget extends Thim_Widget {

	function __construct() {

		parent::__construct(
			'social',
			esc_attr__( 'Thim: Social Links', 'sailing' ),
			array(
				'description' => esc_attr__( 'Social Links', 'sailing' ),
				'help'        => '',
				'panels_groups' => array('thim_widget_group')
			),
			array(),
			array(
				'title'          => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Title', 'sailing' )
				),
				'link_face'      => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Facebook Url', 'sailing' )
				),
				'link_twitter'   => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Twitter Url', 'sailing' )
				),
				'link_google'    => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Google Url', 'sailing' )
				),
				'link_dribble'   => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Dribble Url', 'sailing' )
				),
				'link_linkedin'  => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Linked in Url', 'sailing' )
				),
				'link_pinterest' => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Pinterest Url', 'sailing' )
				),
				'link_digg'      => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Digg Url', 'sailing' )
				),
				'link_youtube'   => array(
					'type'  => 'text',
					'label' => esc_attr__( 'Youtube Url', 'sailing' )
				),
				'link_target'    => array(
					'type'    => 'select',
					'label'   => esc_attr__( 'Link Target', 'sailing' ),
					'options' => array(
						'_self'  => esc_attr__( 'Same window', 'sailing' ),
						'_blank' => esc_attr__( 'New window', 'sailing' ),
					),
				),

			),
			TP_THEME_DIR . 'inc/widgets/social/'
		);
	}

	/**
	 * Initialize the CTA widget
	 */


	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}
}

function thim_social_register_widget() {
	register_widget( 'Thim_Social_Widget' );
}

add_action( 'widgets_init', 'thim_social_register_widget' );