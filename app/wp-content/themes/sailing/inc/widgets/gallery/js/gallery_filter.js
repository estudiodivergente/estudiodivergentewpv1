jQuery(function ($) {
 	$(".thim-widget-gallery .fancybox").fancybox({
	    'margin': 50,
	    'scrolling': 'yes'
    });
	// filter selector
	$(".thim-widget-gallery .filter").on("click", function () {
		var $this = $(this);
		if ( !$this.hasClass("active") ) {
			$(".thim-widget-gallery .filter").removeClass("active");
			$this.addClass("active"); // set the active tab
			// get the data-rel value from selected tab and set as filter
			var $filter = $this.data("rel");
			$filter == 'all' ?
				$(".thim-widget-gallery .fancybox")
					.attr("data-fancybox-group", "gallery")
					.not(":visible")
					.fadeIn()
				: // otherwise
				$(".thim-widget-gallery .fancybox")
					.fadeOut(0)
					.filter(function () {
						return $(this).data("filter") == $filter;
					})
					.attr("data-fancybox-group", $filter)
					.fadeIn(1000);
		}
	});
});