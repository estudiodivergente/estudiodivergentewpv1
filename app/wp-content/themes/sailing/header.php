<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package thim
 */
?><!DOCTYPE html>
<?php
global $thim_options_data;
?>
<html <?php language_attributes(); ?><?php
if ( isset( $thim_options_data['thim_rtl_support'] ) && $thim_options_data['thim_rtl_support'] == '1' ) {
	echo " dir=\"rtl\"";
} ?>
	>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>">
	<?php
	$custom_sticky = $class_header = '';
	if ( isset( $thim_options_data['thim_rtl_support'] ) && $thim_options_data['thim_rtl_support'] == '1' ) {
		$class_header .= 'rtl';
	}
	if ( isset( $thim_options_data['thim_config_att_sticky'] ) && $thim_options_data['thim_config_att_sticky'] == 'sticky_custom' ) {
		$custom_sticky .= ' bg-custom-sticky';
	}
	if ( isset( $thim_options_data['thim_header_sticky'] ) && $thim_options_data['thim_header_sticky'] == 1 ) {
		$custom_sticky .= ' sticky-header';
	}
	if ( isset( $thim_options_data['thim_header_position'] ) ) {
		$custom_sticky .= ' ' . $thim_options_data['thim_header_position'];
		$class_header .= ' wrapper-' . $thim_options_data['thim_header_position'];
	}
	
	$custom_sticky .= ' header_v1';
	$class_header .= ' wrapper-header_v1';

	if ( isset( $thim_options_data['thim_box_layout'] ) && $thim_options_data['thim_box_layout'] =='boxed' ) {
		$class_boxed = 'boxed-area';
	}else{
		$class_boxed = '';
	}

	wp_head();
	?>
</head>

<body <?php body_class( $class_header ); ?>>
<?php if ( isset( $thim_options_data['thim_preload'] ) && $thim_options_data['thim_preload'] == '1' ) { ?>
	<div id="preload">
		<div class="loading-inner">
			<div class="loading loading-1"></div>
			<div class="loading loading-2"></div>
			<div class="loading loading-3"></div>
			<div class="loading loading-4"></div>
			<div class="loading loading-5"></div>
			<div class="loading loading-6"></div>
			<div class="loading loading-7"></div>
			<div class="loading loading-8"></div>
			<div class="loading loading-9"></div>
		</div>
	</div>
<?php } ?>
<div id="wrapper-container" class="wrapper-container">
	<div class="content-pusher <?php echo esc_attr($class_boxed); ?>">
		<header id="masthead" class="site-header affix-top<?php echo esc_attr( $custom_sticky ); ?>">
			<?php
			// Drawer
			if ( isset( $thim_options_data['thim_show_drawer'] ) && $thim_options_data['thim_show_drawer'] == '1' && is_active_sidebar( 'drawer_top' ) ) {
				get_template_part( 'inc/header/drawer' );
			}
			
			get_template_part( 'inc/header/header_v1' );
			
			?>
		</header>
		<div id="main-content">
