== Upgrade Notice ==

= 1.0.1 =
1. Fix load text-domain.
2. Add language file en_US
3. Add Extra Package
4. Update, Fix Query search ( customer's feedback: http://thimpress.com/forums/topic/calendar-availabality-room/#post-42595 )
5. Query search booking_pricing( Admin panel )
6. Woocommerce Payment( Addon )

= 1.0.2 =
1. Update Woocommerce addon
2. Search rooms. Import demo data more than once. post_meta not unique.

= 1.0.3 =
1. Update, Add hook ajax search url result. 'hotel_booking_parse_search_param', 'hotel_booking_ajax_remove_cart_item'
2. Addon Block Special Date( Angularjs, ajax )
3. Update Rating

= 1.1 =
1. Add Sessions Class, Update Cart Class
2. Optimize process checkout
3. Remove data saving booking if it not usesful( postmeta )
4. Optimize Payment Stripe process( remove stripe sdk, use REST API )
5. Admin booking details
6. Update hook processs addon

= 1.1.1 =
1. Update Resize image, gallery, archive rooms
2. Update Session class
3. Support Loco Translation & Mulilanguages