<?php
/**
 * Loop Price
 *
 * @author 		ThimPress
 * @package 	Tp-hotel-booking/Templates
 * @version     0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $hb_settings;
$price_display = apply_filters( 'hotel_booking_loop_room_price_display_style', $hb_settings->get('price_display') );

$currency = hb_get_currency_symbol();
$prices = hb_get_price_plan_room(get_the_ID());
?>
<?php if( $prices ): ?>
	<?php
		$min = current($prices);
		$max = end($prices);
	?>
	<div class="price">
		<span class="title-price"><?php _e( 'Price', 'tp-hotel-booking' ); ?></span>
		<?php if( $price_display === 'max' ): ?>

			<span class="price_value price_max"><?php echo $currency ?><?php echo $max ?></span>

		<?php elseif( $price_display === 'min_to_max' && $min !== $max ): ?>

			<span class="price_value price_min_to_max">
				<?php echo $currency; ?><?php echo $min ?>
				-
				<?php echo $currency; ?><?php echo $max; ?>
			</span>

		<?php else: ?>

			<span class="price_value price_min"><?php echo $currency; ?><?php echo $min ?></span>

		<?php endif; ?>
		<span class="unit"><?php _e( 'Night', 'tp-hotel-booking' ); ?></span>
	</div>
<?php endif; ?>