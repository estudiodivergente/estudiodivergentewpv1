<?php
/**
 * template extra admin cart
 * @since  1.1
 */
?>
<tr class="hb_addition_services_title hb_table_center booking-table-row">
	<td colspan="6" style="text-align: center; font-weight: bold">
		<?php _e( 'Addition Services', 'tp-hotel-booking' ); ?>
	</td>
</tr>
