<?php

$notification = isset( $_REQUEST['notification'] ) ? $_REQUEST['notification'] : '';

ob_start();
if ( $notification == 'warring_overwritten' ) {
	$count_dismiss = intval( get_option( 'tp_importer_warring_overwritten', 0 ) );
	$count_dismiss ++;
	$updated = update_option( 'tp_importer_warring_overwritten', $count_dismiss );

	if ( $updated ) {
		echo json_encode( array(
			'return' => true,
			'msg'    => '',
			'log'    => ob_get_clean()
		) );
		exit();
	}
}